# FlinkKafkaEventTimeMWE

While developing a module to find patterns with Flink within data from a Kafka topic the pattern matching code is never called. This behaviour is reproduced with this code. 

## Requirements
* Java 8

## Deployment
Simply execute the `patternTest` in the `KafkaFlinkPatternIntegrationTest` class. It will print some `Interval`s that it finds within the input data but it never executes the `Pattern` code.