import com.salesforce.kafka.test.KafkaTestUtils;
import com.salesforce.kafka.test.junit4.SharedKafkaTestResource;
import de.lr.mwe.Interval;
import de.lr.mwe.TestData;
import de.lr.mwe.serde.*;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumerBase;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KafkaFlinkPatternIntegrationTest {

    @ClassRule
    public static final SharedKafkaTestResource sharedKafkaTestResource = new SharedKafkaTestResource();
    @Rule
    public Timeout globalTimeout = Timeout.seconds(20);

    private String currentTopic;

    @Before
    public void setUp() {
        currentTopic = UUID.randomUUID().toString();
        getKafkaTestUtils().createTopic(currentTopic, 2, (short) 1);
        CollectSink.values.clear();
    }

    @Test
    public void producerTest() {
        List<ProducerRecord<String, TestData>> records = Arrays.asList(
                new ProducerRecord<>(currentTopic, new TestData(0.3)),
                new ProducerRecord<>(currentTopic, new TestData(0.2)),
                new ProducerRecord<>(currentTopic, new TestData(0.1)),
                new ProducerRecord<>(currentTopic, new TestData(0.0)),
                new ProducerRecord<>(currentTopic, new TestData(0.0)),
                new ProducerRecord<>(currentTopic, new TestData(0.1)),
                new ProducerRecord<>(currentTopic, new TestData(0.2)),
                new ProducerRecord<>(currentTopic, new TestData(0.3)),
                new ProducerRecord<>(currentTopic, new TestData(0.2)),
                new ProducerRecord<>(currentTopic, new TestData(0.1)),
                new ProducerRecord<>(currentTopic, new TestData(0.0))
        );

        try (KafkaProducer<String, TestData> producer =
                     getKafkaTestUtils().getKafkaProducer(StringSerializer.class, TestDataSerializer.class)) {
            records.forEach(producer::send);
        }

        List<ConsumerRecord<String, TestData>> result = new ArrayList<>();
        try (final KafkaConsumer<String, TestData> kafkaConsumer =
                     getKafkaTestUtils().getKafkaConsumer(StringDeserializer.class, TestDataDeserializer.class)) {

            final List<TopicPartition> topicPartitionList = new ArrayList<>();
            for (final PartitionInfo partitionInfo : kafkaConsumer.partitionsFor(currentTopic)) {
                topicPartitionList.add(new TopicPartition(partitionInfo.topic(), partitionInfo.partition()));
            }
            kafkaConsumer.assign(topicPartitionList);
            kafkaConsumer.seekToBeginning(topicPartitionList);

            // Pull records from kafka, keep polling until we get nothing back
            ConsumerRecords<String, TestData> consumerRecords;
            do {
                consumerRecords = kafkaConsumer.poll(1000L);
                consumerRecords.forEach(result::add);
            }
            while (!consumerRecords.isEmpty());
        }

        assertEquals(records.size(), result.size());
    }

    @Test
    public void patternTest() throws Exception {
        List<Double> values = Arrays.asList(
                0.3,
                0.2,
                0.1,
                0.0,
                0.0,
                0.1,
                0.2,
                0.3,
                0.2,
                0.1,
                0.0,
                null // Stop signal
        );

        try (KafkaProducer<String, TestData> producer =
                     getKafkaTestUtils().getKafkaProducer(StringSerializer.class, TestDataSerializer.class)) {
            AtomicLong dummyTime = new AtomicLong(0);
            values.forEach(value -> producer.send(
                    new ProducerRecord<>(currentTopic, new TestData(value, dummyTime.incrementAndGet()))
            ));
        }

        FlinkKafkaConsumerBase<TestData> consumer =
                new FlinkKafkaConsumer011<>(currentTopic, new TestDataDeserializationSchema(), getKafkaProperties())
                        .setStartFromEarliest()
                        .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<TestData>(Time.seconds(1L)) {
                            private static final long serialVersionUID = 1L;

                            @Override
                            public long extractTimestamp(TestData element) {
                                return element.getEventTime();
                            }
                        });

        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStreamSource<TestData> robotDataDataStreamSource = executionEnvironment.addSource(consumer);

        SingleOutputStreamOperator<Interval> intervals = robotDataDataStreamSource
                .filter((FilterFunction<TestData>) Objects::nonNull)
                .name("Filter null values from processing stream")
                .countWindowAll(2, 1)
                .process(new ProcessAllWindowFunction<TestData, Interval, GlobalWindow>() {
                    @Override
                    public void process(Context context, Iterable<TestData> elements, Collector<Interval> out) {
                        List<TestData> forces = new ArrayList<>();
                        elements.forEach(forces::add);
                        if (forces.size() == 2) {
                            TestData a = forces.get(0);
                            TestData b = forces.get(1);
                            if (a.getEventTime() < b.getEventTime()) {
                                out.collect(new Interval(a, b, true));
                            } else {
                                out.collect(new Interval(b, a, true));
                            }
                        }
                    }
                })
                .name("Create intervals from two successive input data points");

        intervals.print();

        Pattern<Interval, Interval> decreasing = Pattern
                .<Interval>begin("START")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                })
                .oneOrMore()
                .next("STOP")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return !value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                });

        CEP.pattern(intervals, decreasing)
                .select(new PatternSelectFunction<Interval, Interval>() {
                    @Override
                    public Interval select(Map<String, List<Interval>> pattern) {
                        List<Interval> select = pattern.get("START");
                        Interval interval = new Interval(
                                select.get(0).getStart(),
                                select.get(select.size() - 1).getEnd(),
                                false);
                        System.out.println("Detected decreasing pattern: " + interval);
                        return interval;
                    }
                })
                .name("Select decreasing intervals")
                .addSink(new CollectSink())
                .name("Collect result");

        executionEnvironment.execute();

        assertTrue("Pattern should be detected at least once", CollectSink.values.size() > 0);
    }

    @Test
    public void patternIntervalTest() throws Exception {
        AtomicLong dummyTime = new AtomicLong(0);
        List<Interval> values = Arrays.asList(
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                null // Stop signal
        );

        System.out.println("Sending test data to Kafka:");
        try (KafkaProducer<String, Interval> producer =
                     getKafkaTestUtils().getKafkaProducer(StringSerializer.class, IntervalSerializer.class)) {
            values.forEach(value -> {
                producer.send(new ProducerRecord<>(currentTopic, value));
                System.out.println("\t" + value);
            });
        }

        FlinkKafkaConsumerBase<Interval> consumer =
                new FlinkKafkaConsumer011<>(currentTopic, new IntervalDeserializationSchema(), getKafkaProperties())
                        .setStartFromEarliest()
                        .assignTimestampsAndWatermarks(
                                new BoundedOutOfOrdernessTimestampExtractor<Interval>(Time.seconds(1L)) {
                                    private static final long serialVersionUID = 1L;

                                    @Override
                                    public long extractTimestamp(Interval element) {
                                        return element.getEnd().getEventTime();
                                    }
                                });

        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        SingleOutputStreamOperator<Interval> intervals = executionEnvironment.addSource(consumer)
                .filter((FilterFunction<Interval>) value -> value != null);

        intervals.print();

        Pattern<Interval, Interval> mounted = Pattern
                .<Interval>begin("PRE_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                })
                .next("INCREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.INCREASING);
                    }
                })
                .next("POST_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                });

        CEP.pattern(intervals, mounted)
                .select(new PatternSelectFunction<Interval, Interval>() {
                    @Override
                    public Interval select(Map<String, List<Interval>> pattern) {
                        List<Interval> intervals = pattern.get("START");
                        Interval interval = new Interval(
                                intervals.get(0).getStart(),
                                intervals.get(intervals.size() - 1).getEnd(),
                                false);
                        System.out.println("Detected mounted pattern: " + interval);
                        return interval;
                    }
                })
                .name("Select result")
                .addSink(new CollectSink())
                .name("Collect result");

        executionEnvironment.execute();

        assertTrue("Pattern should be detected at least once", CollectSink.values.size() > 0);
    }

    @Test
    public void patternKeyedIntervalTest() throws Exception {
        AtomicLong dummyTime = new AtomicLong(0);
        List<Interval> values = Arrays.asList(
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                null // Stop signal
        );

        System.out.println("Sending test data to Kafka:");
        try (KafkaProducer<String, Interval> producer =
                     getKafkaTestUtils().getKafkaProducer(StringSerializer.class, IntervalSerializer.class)) {
            values.forEach(value -> {
                producer.send(new ProducerRecord<>(currentTopic, value));
                System.out.println("\t" + value);
            });
        }

        FlinkKafkaConsumerBase<Interval> consumer =
                new FlinkKafkaConsumer011<>(currentTopic, new IntervalDeserializationSchema(), getKafkaProperties())
                        .setStartFromEarliest()
                        .assignTimestampsAndWatermarks(
                                new BoundedOutOfOrdernessTimestampExtractor<Interval>(Time.seconds(1L)) {
                                    private static final long serialVersionUID = 1L;

                                    @Override
                                    public long extractTimestamp(Interval element) {
                                        return element.getEnd().getEventTime();
                                    }
                                });

        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        KeyedStream<Interval, String> keyedIntervals = executionEnvironment.addSource(consumer)
                .filter((FilterFunction<Interval>) value -> value != null)
                .keyBy(new KeySelector<Interval, String>() {

                    @Override
                    public String getKey(Interval value) {
                        return "interval";
                    }
                });

        keyedIntervals.print();

        Pattern<Interval, Interval> mounted = Pattern
                .<Interval>begin("PRE_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                })
                .next("INCREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.INCREASING);
                    }
                })
                .next("POST_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                });

        CEP.pattern(keyedIntervals, mounted)
                .select(new PatternSelectFunction<Interval, Interval>() {
                    @Override
                    public Interval select(Map<String, List<Interval>> pattern) {
                        List<Interval> intervals = pattern.get("START");
                        Interval interval = new Interval(
                                intervals.get(0).getStart(),
                                intervals.get(intervals.size() - 1).getEnd(),
                                false);
                        System.out.println("Detected mounted pattern: " + interval);
                        return interval;
                    }
                })
                .name("Select result")
                .addSink(new CollectSink())
                .name("Collect result");

        executionEnvironment.execute();

        assertTrue("Pattern should be detected at least once", CollectSink.values.size() > 0);
    }

    @Test
    public void patternIntervalLateWatermarkTest() throws Exception {
        AtomicLong dummyTime = new AtomicLong(0);
        List<Interval> values = Arrays.asList(
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(1.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(1.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                new Interval(
                        new TestData(0.0, dummyTime.getAndIncrement()),
                        new TestData(0.0, dummyTime.get()),
                        false),
                null // Stop signal
        );

        System.out.println("Sending test data to Kafka:");
        try (KafkaProducer<String, Interval> producer =
                     getKafkaTestUtils().getKafkaProducer(StringSerializer.class, IntervalSerializer.class)) {
            values.forEach(value -> {
                producer.send(new ProducerRecord<>(currentTopic, value));
                System.out.println("\t" + value);
            });
        }

        FlinkKafkaConsumerBase<Interval> consumer =
                new FlinkKafkaConsumer011<>(currentTopic, new IntervalDeserializationSchema(), getKafkaProperties())
                        .setStartFromEarliest();

        StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        executionEnvironment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        // Generate automatic watermarks after 5 seconds
        executionEnvironment.getConfig().setAutoWatermarkInterval(5000L);

        SingleOutputStreamOperator<Interval> intervals = executionEnvironment.addSource(consumer)
                .filter((FilterFunction<Interval>) value -> value != null);

        intervals.print();

        Pattern<Interval, Interval> mounted = Pattern
                .<Interval>begin("PRE_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                })
                .next("INCREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.INCREASING);
                    }
                })
                .next("POST_DECREASING")
                .where(new SimpleCondition<Interval>() {
                    @Override
                    public boolean filter(Interval value) {
                        return value.getType().equals(Interval.IntervalType.DECREASING);
                    }
                });

        CEP.pattern(intervals, mounted)
                .select(new PatternSelectFunction<Interval, Interval>() {
                    @Override
                    public Interval select(Map<String, List<Interval>> pattern) {
                        List<Interval> intervals = pattern.get("START");
                        Interval interval = new Interval(
                                intervals.get(0).getStart(),
                                intervals.get(intervals.size() - 1).getEnd(),
                                false);
                        System.out.println("Detected mounted pattern: " + interval);
                        return interval;
                    }
                })
                .name("Select result")
                .addSink(new CollectSink())
                .name("Collect result");

        executionEnvironment.execute();

        assertTrue("Pattern should be detected at least once", CollectSink.values.size() > 0);
    }

    private KafkaTestUtils getKafkaTestUtils() {
        return sharedKafkaTestResource.getKafkaTestUtils();
    }

    private Properties getKafkaProperties() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                sharedKafkaTestResource.getKafkaBrokers().asList().get(0).getConnectString());
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        return properties;
    }

    private static class CollectSink implements SinkFunction<Interval> {

        static final List<Interval> values = new ArrayList<>();

        @Override
        public void invoke(Interval value, Context context) {
            values.add(value);
        }

    }

}
