package de.lr.mwe;

import java.util.Date;

public class TestData {

    private Double value;
    private Long eventTime;

    public TestData(Double value) {
        this(value, new Date().getTime());
    }

    public TestData(Double value, Long eventTime) {
        this.value = value;
        this.eventTime = eventTime;
    }

    public Double getValue() {
        return value;
    }

    public Long getEventTime() {
        return eventTime;
    }

    @Override
    public String toString() {
        return String.format("TestData(value: %s, eventTime: %s)", value.toString(), eventTime.toString());
    }
}
