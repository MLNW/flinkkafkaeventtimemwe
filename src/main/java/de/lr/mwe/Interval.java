package de.lr.mwe;

import org.apache.flink.annotation.VisibleForTesting;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Interval implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Double EPSILON = 0.03;
    private static final Format format = new SimpleDateFormat("HH:mm:ss:SS");
    private static final DecimalFormat df = new DecimalFormat("#.000000");
    private final TestData start;
    private final TestData end;
    private final Double slope;
    private final IntervalType type;

    public Interval(TestData start, TestData end, boolean constantSampleRate) {
        this.start = start;
        this.end = end;

        slope = constantSampleRate ?
                calculateSlopeConstantTime(start, end) : calculateSlope(start, end);

        if (slope < -EPSILON) {
            type = IntervalType.DECREASING;
        } else if (slope > 2 * EPSILON) {
            type = IntervalType.INCREASING;
        } else {
            type = IntervalType.STABLE;
        }
    }

    @VisibleForTesting
    static Double calculateValueDifference(Double start, Double end) {
        assert start != null && end != null;
        return end - start;
    }

    @VisibleForTesting
    static Long calculateDuration(Long start, Long end) {
        assert start != null && end != null;
        return end - start;
    }

    @VisibleForTesting
    static Double calculateSlope(TestData start, TestData end) {
        assert start != null && end != null;
        return calculateValueDifference(start.getValue(), end.getValue())
                / calculateDuration(start.getEventTime(), end.getEventTime());
    }

    ////////////////////////
    // Delomboked Methods //
    ////////////////////////

    @VisibleForTesting
    static Double calculateSlopeConstantTime(TestData start, TestData end) {
        return calculateValueDifference(start.getValue(), end.getValue());
    }

    public TestData getStart() {
        return start;
    }

    public TestData getEnd() {
        return end;
    }

    public Double getSlope() {
        return slope;
    }

    public IntervalType getType() {
        return type;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Interval)) return false;
        final Interval other = (Interval) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$start = this.getStart();
        final Object other$start = other.getStart();
        if (this$start == null ? other$start != null : !this$start.equals(other$start)) return false;
        final Object this$end = this.getEnd();
        final Object other$end = other.getEnd();
        if (this$end == null ? other$end != null : !this$end.equals(other$end)) return false;
        final Object this$slope = this.getSlope();
        final Object other$slope = other.getSlope();
        if (this$slope == null ? other$slope != null : !this$slope.equals(other$slope)) return false;
        final Object this$type = this.getType();
        final Object other$type = other.getType();
        return this$type == null ? other$type == null : this$type.equals(other$type);
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $start = this.getStart();
        result = result * PRIME + ($start == null ? 43 : $start.hashCode());
        final Object $end = this.getEnd();
        result = result * PRIME + ($end == null ? 43 : $end.hashCode());
        final Object $slope = this.getSlope();
        result = result * PRIME + ($slope == null ? 43 : $slope.hashCode());
        final Object $type = this.getType();
        result = result * PRIME + ($type == null ? 43 : $type.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Interval;
    }

    public String toString() {
        return "Interval(start=" + convertTime(start.getEventTime())
                + ", end=" + convertTime(end.getEventTime())
                + ", duration=" + calculateDuration(start.getEventTime(), end.getEventTime())
                + ", slope=" + df.format(getSlope())
                + ", type=" + getType() + ")";
    }

    private String convertTime(long time) {
        Date date = new Date(time);
        return format.format(date);
    }

    public enum IntervalType {
        INCREASING,
        DECREASING,
        STABLE
    }

}
