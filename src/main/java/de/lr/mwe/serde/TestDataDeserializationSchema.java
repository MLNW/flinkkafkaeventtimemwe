package de.lr.mwe.serde;

import de.lr.mwe.TestData;
import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;

public class TestDataDeserializationSchema extends AbstractDeserializationSchema<TestData> {

    private final TestDataDeserializer deserializer = new TestDataDeserializer();

    @Override
    public TestData deserialize(byte[] message) {
        return deserializer.deserialize(null, message);
    }

    @Override
    public boolean isEndOfStream(TestData nextElement) {
        return nextElement == null;
    }

}
