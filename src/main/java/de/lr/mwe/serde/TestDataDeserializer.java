package de.lr.mwe.serde;

import com.google.gson.GsonBuilder;
import de.lr.mwe.TestData;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.Serializable;
import java.util.Map;

public class TestDataDeserializer implements Deserializer<TestData>, Serializable {

    @Override
    public void configure(Map<String, ?> map, boolean b) {
        // nothing to do
    }

    @Override
    public TestData deserialize(String topic, byte[] data) {
        return data == null ? null : new GsonBuilder().create().fromJson(new String(data), TestData.class);
    }

    @Override
    public void close() {
        // nothing to do
    }

}
