package de.lr.mwe.serde;

import com.google.gson.GsonBuilder;
import de.lr.mwe.Interval;
import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;

public class IntervalDeserializationSchema extends AbstractDeserializationSchema<Interval> {

    @Override
    public Interval deserialize(byte[] message) {
        return message == null ? null : new GsonBuilder().create().fromJson(new String(message), Interval.class);
    }

    @Override
    public boolean isEndOfStream(Interval nextElement) {
        return nextElement == null;
    }

}
