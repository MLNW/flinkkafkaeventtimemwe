package de.lr.mwe.serde;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.lr.mwe.TestData;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class TestDataSerializer implements Serializer<TestData> {

    private Gson gson;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        gson = new GsonBuilder().create();
    }

    @Override
    public byte[] serialize(String topic, TestData data) {
        return data.getValue() == null ? null : gson.toJson(data).getBytes();
    }

    @Override
    public void close() {
        // nothing to do
    }
}
