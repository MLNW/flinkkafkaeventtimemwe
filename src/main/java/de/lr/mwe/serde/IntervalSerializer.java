package de.lr.mwe.serde;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.lr.mwe.Interval;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class IntervalSerializer implements Serializer<Interval> {

    private Gson gson;

    @Override
    public void configure(Map<String, ?> map, boolean b) {
        gson = new GsonBuilder().create();
    }

    @Override
    public byte[] serialize(String topic, Interval interval) {
        return interval == null ? null : gson.toJson(interval).getBytes();
    }

    @Override
    public void close() {
        // nothing do to
    }
}
